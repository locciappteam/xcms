using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace XCMS.WorkerService
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private HttpClient client; 

        public Worker(ILogger<Worker> logger)
        {
            _logger = logger;
        }

        public override Task StartAsync(CancellationToken cancellationToken)
        {
            client = new HttpClient();
            return base.StartAsync(cancellationToken);
        }

        public override Task StopAsync(CancellationToken cancellationToken)
        {
            client.Dispose();
            return base.StopAsync(cancellationToken);
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
               var result = await client.GetAsync("https://www.loccitane.com");
                if (result.IsSuccessStatusCode)
                {
                    _logger.LogInformation("The website was up! {StatusCode}", result.StatusCode);
                }
                else
                {
                    _logger.LogError("The website is down {StatusCode}", result.StatusCode);
                }

               await Task.Delay(5000, stoppingToken);
            }
        }
    }
}
