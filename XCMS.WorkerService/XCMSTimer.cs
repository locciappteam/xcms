﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;
using XCMS.Core.Import;

namespace XCMS.WorkerService
{
    public class XCMSTimer : BackgroundService
    {
        private readonly ILogger<XCMSTimer> _logger;

        public XCMSTimer(ILogger<XCMSTimer> logger)
        {
            _logger = logger;
        }

        public override Task StartAsync(CancellationToken cancellationToken)
        {
            return base.StartAsync(cancellationToken);
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while(!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    Task.Run(async () =>
                    {
                        await Import.OrdersProvenceCSV("OCMS_Orders_", @"D:\ORDERS\", @"D:\ORDERS\Backup\");
                        await Import.OrdersTrackingProvenceCSV("OCMS_OrdersTracking_", @"D:\ORDERS\", @"D:\ORDERS\Backup\");

                    }).Wait();

                    _logger.LogInformation("Importação completada com sucesso {Now}!", DateTime.Now);

                }
                catch (Exception ex)
                {
                    _logger.LogError(ex + ": Deu ruim na Importação{Now}!", DateTime.Now);
                }

                await Task.Delay(5000, stoppingToken);
            }
        }

        public override Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("O serviço foi parado...");
            return base.StopAsync(cancellationToken);
        }
    }
}
