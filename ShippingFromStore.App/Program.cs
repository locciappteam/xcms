﻿using System;
using System.Text;
using System.Threading.Tasks;
using XCMS.Data.Context;

namespace ShippingFromStore.App
{
    class Program
    {
        static void Main(string[] args)
        {

            try
            {
                Console.WriteLine(args[0]);

                string User;
                string Pass;
                Uri baseAddress;

                using (ContextDatabase _ctx = new ContextDatabase("PORTALLOCCITANE"))
                {
                    User = _ctx.wmsParametros.Find("USUARIO_API").VALOR;
                    Pass = _ctx.wmsParametros.Find("SENHA_API").VALOR;
                    baseAddress = new Uri(_ctx.wmsParametros.Find("BASE_ADDRESS").VALOR);
                }

                byte[] byteAuth = Encoding.ASCII.GetBytes($"{User}:{Pass}");
                string Auth = Convert.ToBase64String(byteAuth);

                Task.Run(async () =>
                {
                    //await XCMS.Core.ShippingFromStore.ShippingFromStore.GetTmpLineItemAsync(args[0], baseAddress, Auth);

                }).Wait();

            }
            catch (Exception ex)
            {
                Console.WriteLine("Erro ao Executar: " + ex.Message);
                Console.ReadLine();
            }
        }

    }

}