﻿using System;
using System.Reflection;

namespace XCMS.Data.Model.Integration.Excel
{
    public class OrderSQLQuery
    {

        public long Order { get; set; }
        public long Customer { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public short NbLine { get; set; }
        public decimal? OrderTotal { get; set; }
        public decimal? ShippingFee { get; set; }
        public decimal? Tax { get; set; }
        public decimal? GlobalTotal { get; set; }
        public string Status { get; set; }
        public string Comment { get; set; }
        public DateTime? DateCreated { get; set; }
        public string Info { get; set; }
        public string Tracking { get; set; }
        public string ShippingMethod { get; set; }
        public string PaymentMethod { get; set; }
        public string AuthNumber { get; set; }
        public string TransactionID { get; set; }
        public string CardType { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime? DateModified { get; set; }
        public decimal? DiscountTotal { get; set; }
        public string ShippingFirstName { get; set; }
        public string ShippingLastName { get; set; }
        public string ShippingPhone { get; set; }
        public string ShippingZip { get; set; }
        public string ShippingCity { get; set; }
        public string ShippingState { get; set; }
        public string CustomerOptional1 { get; set; }
        public string CustomerOptional2 { get; set; }
        public string CustomerOptional3 { get; set; }
        public string CustomerOptional4 { get; set; }
        public string CustomerOptional5 { get; set; }
        public string CustomerAddress { get; set; }
        public string CustomerAddress2 { get; set; }
        public string CustomerAddress3 { get; set; }
        public string CustomerAddress4 { get; set; }
        public string CustomerAddress5 { get; set; }
        public string CustomerAddress6 { get; set; }
        public string CustomerAddress7 { get; set; }
        public string CustomerAddress8 { get; set; }
        public string CustomerAddress9 { get; set; }
        public string ShippingAddress { get; set; }
        public string ShippingAddress2 { get; set; }
        public string ShippingAddress3 { get; set; }
        public string ShippingAddress4 { get; set; }
        public string ShippingAddress5 { get; set; }
        public string ShippingAddress6 { get; set; }
        public string ShippingAddress7 { get; set; }
        public string ShippingAddress8 { get; set; }
        public string ShippingAddress9 { get; set; }
        public string Source { get; set; }
        public string GiftMessage { get; set; }
        public string GiftInstructions { get; set; }
    }
}
