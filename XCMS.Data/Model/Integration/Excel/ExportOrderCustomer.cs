﻿namespace XCMS.Data.Model.Integration.Excel
{
    public class ExportOrderCustomer
    {
        public string type { get; set; }
        public string r_Tiers { get; set; }
        public string t_Etabcreation { get; set; }
        public string t_Juridique { get; set; }
        public string t_Libelle { get; set; }
        public string t_Prenom { get; set; }
        public string t_Adresse1 { get; set; }
        public string t_Adresse2 { get; set; }
        public string t_Adresse3 { get; set; }
        public string t_Ville { get; set; }
        public string t_Region { get; set; }
        public string t_CodePostal { get; set; }
        public string t_Pays { get; set; }
        public string t_Telephone { get; set; }
        public string t_Telex { get; set; }
        public string t_Telephone2 { get; set; }
        public string t_Email { get; set; }
        public string t_Journaissance { get; set; }
        public string t_MoisNaissance { get; set; }
        public string t_AnneeNaissance { get; set; }
        public string t_Sexe { get; set; }
        public string t_Etablissement { get; set; }
        public string t_Langue { get; set; }
        public string t_Auxiliaire { get; set; }
        public string t_NatureAuxi { get; set; }
        public string t_Devise { get; set; }
        public string t_Emailing { get; set; }
        public string ytc_CodeCpfCnpj { get; set; }
    }
}
