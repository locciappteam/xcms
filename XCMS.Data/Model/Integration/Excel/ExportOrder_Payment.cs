﻿namespace XCMS.Data.Model.Integration.Excel
{
    public class ExportOrderPayment
    {
        public string recordType  { get; set; }
        public string internalReference { get; set; }
        public string numero { get; set; }
        public string paymentCode { get; set; }
        public string amount { get; set; }
        public string currency { get; set; }
    }
}
