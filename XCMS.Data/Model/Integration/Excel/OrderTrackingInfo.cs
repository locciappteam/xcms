﻿using System;

namespace XCMS.Data.Model.Integration.Excel
{
    public class OrderTrackingInfo
    {
        public long OrderId { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime LastModification { get; set; }
        public string Name { get; set; }
        public string StatusFrom { get; set; }
        public string StatusTo { get; set; }
        public string OrderInfo { get; set; }
    }
}
