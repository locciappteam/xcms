﻿namespace XCMS.Data.Model.Integration.Excel
{
    public class ExportOrderLineItem
    {
        public string recordType {get;set;}
        public string store {get;set;}
        public string cais {get;set;}
        public string orderNumber {get;set;}
        public string date {get;set;}
        public string client {get;set;}
        public string currency {get;set;}
        public string itemNumber {get;set;}
        public string quantitySecomOrderLined {get;set;}
        public string unitPrice {get;set;}
        public string totalPriceLine {get;set;}
        public string totalPriceDiscountedLine {get;set;}
        public string markdownReason {get;set;}
        public string footerDiscount {get;set;}
        public string taxSystem {get;set;}
        public string zipCode {get;set;}
        public string carrier {get;set;}
        public string isGift {get;set;}
        public string volume { get; set; }

    }
}
