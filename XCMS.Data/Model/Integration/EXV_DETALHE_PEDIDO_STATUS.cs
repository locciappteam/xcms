﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace XCMS.Data.Model.Integration
{
    [Table("EXV_DETALHES_PEDIDOS_STATUS")]
    public class EXV_DETALHE_PEDIDO_STATUS : DefaultEntity
    {
        public int ID { get; set; }
        public string PEDIDO_EXTERNO { get; set; }
        public string STATUS { get; set; }
        public DateTime DATA { get; set; }
        public string PEDIDO_WMS { get; set; }
        public string STATUS_DESCRICAO { get; set; }
        public virtual EXV_STATUS_PEDIDO EXV_STATUS_PEDIDO { get; set; }
    }
}
