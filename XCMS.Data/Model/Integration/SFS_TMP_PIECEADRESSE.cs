﻿namespace XCMS.Data.Model.Integration
{
    public class SFS_TMP_PIECEADRESSE : DefaultEntity
    {
        public string RecordType { get; set; }

        public string RefInterne { get; set; }

        public string TypePieceAd { get; set; }

        public string Libelle { get; set; }

        public string Libelle2 { get; set; }

        public string Adresse1 { get; set; }

        public string Adresse2 { get; set; }

        public string Adresse3 { get; set; }

        public string CodePostal { get; set; }

        public string Ville { get; set; }

        public string Region { get; set; }

        public string IntegrationOrigin { get; set; }
    }
}
