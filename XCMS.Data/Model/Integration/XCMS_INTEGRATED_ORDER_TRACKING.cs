﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace XCMS.Data.Model.Integration
{
    [Table("XCMS_INTEGRATED_ORDERS_TRACKING")]
    public class XCMS_INTEGRATED_ORDER_TRACKING
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long ID { get; set; }

        public long ORDERID { get; set; }
        public DateTime CREATIONDATE { get; set; }
        public DateTime LASTMODIFICATION { get; set; }
        public string NAME { get; set; }
        public string STATUSFROM { get; set; }
        public string STATUSTO { get; set; }
        public string ORDERINFO { get; set; }
    }
}
