﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace XCMS.Data.Model.Integration
{
    [Table("XCMS_INTEGRATED_ORDERS")]
    public class XCMS_INTEGRATED_ORDER
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long ORDERID { get; set; }

        public byte MARCAID { get; set; }
        public long CUSTOMERID { get; set; }
        public string EMAIL { get; set; }
        public string FIRSTNAME { get; set; }
        public string LASTNAME { get; set; }
        public short NBLINE { get; set; }
        public decimal? ORDERTOTAL { get; set; }
        public decimal? SHIPPINGFEE { get; set; }
        public decimal? TAX { get; set; }
        public decimal? GLOBALTOTAL { get; set; }
        public string STATUS { get; set; }
        public string COMMENT { get; set; }
        public DateTime? DATECREATED { get; set; }
        public string INFO { get; set; }
        public string TRACKING { get; set; }
        public string SHIPPINGMETHOD { get; set; }
        public string PAYMENTMETHOD { get; set; }
        public string AUTHNUMBER { get; set; }
        public string TRANSACTIONID { get; set; }
        public string CARDTYPE { get; set; }
        public string LASTMODIFIEDBY { get; set; }
        public DateTime? DATEMODIFIED { get; set; }
        public decimal? DISCOUNTTOTAL { get; set; }
        public string SHIPPINGFIRSTNAME { get; set; }
        public string SHIPPINGLASTNAME { get; set; }
        public string SHIPPINGPHONE { get; set; }
        public string SHIPPINGZIP { get; set; }
        public string SHIPPINGCITY { get; set; }
        public string SHIPPINGSTATE { get; set; }
        public string CUSTOMEROPTIONAL1 { get; set; }
        public string CUSTOMEROPTIONAL2 { get; set; }
        public string CUSTOMEROPTIONAL3 { get; set; }
        public string CUSTOMEROPTIONAL4 { get; set; }
        public string CUSTOMEROPTIONAL5 { get; set; }
        public string CUSTOMERADDRESS { get; set; }
        public string CUSTOMERADDRESS2 { get; set; }
        public string CUSTOMERADDRESS3 { get; set; }
        public string CUSTOMERADDRESS4 { get; set; }
        public string CUSTOMERADDRESS5 { get; set; }
        public string CUSTOMERADDRESS6 { get; set; }
        public string CUSTOMERADDRESS7 { get; set; }
        public string CUSTOMERADDRESS8 { get; set; }
        public string CUSTOMERADDRESS9 { get; set; }
        public string SHIPPINGADDRESS { get; set; }
        public string SHIPPINGADDRESS2 { get; set; }
        public string SHIPPINGADDRESS3 { get; set; }
        public string SHIPPINGADDRESS4 { get; set; }
        public string SHIPPINGADDRESS5 { get; set; }
        public string SHIPPINGADDRESS6 { get; set; }
        public string SHIPPINGADDRESS7 { get; set; }
        public string SHIPPINGADDRESS8 { get; set; }
        public string SHIPPINGADDRESS9 { get; set; }
        public string SOURCE { get; set; }
        public string GIFTMESSAGE { get; set; }
        public string GIFTINSTRUCTIONS { get; set; }
        public string MUPID { get; set; }
    }
}
