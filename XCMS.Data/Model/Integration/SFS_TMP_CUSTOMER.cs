﻿using System.ComponentModel.DataAnnotations.Schema;

namespace XCMS.Data.Model.Integration
{
    [Table("SFS_TMP_CUSTOMERS")]
    public class SFS_TMP_CUSTOMER : DefaultEntity
    {
        public string Type { get; set; }

        public string VipCode { get; set; }

        public string StoreCode { get; set; }

        public string Title { get; set; }

        public string LastName { get; set; }

        public string FirstName { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string Address3 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string PostCode { get; set; }

        public string Country { get; set; }

        public string Phone { get; set; }

        public string Mobile { get; set; }

        public string Phone3 { get; set; }

        public string Email { get; set; }

        public string Daybirth { get; set; }

        public string MonthBirth { get; set; }

        public string YearBirth { get; set; }

        public string Sex { get; set; }

        public string T_Etablissement { get; set; }

        public string T_Langue { get; set; }

        public string T_Auxiliaire { get; set; }

        public string T_TvaEncaissement { get; set; }

        public string T_NatureAuxi { get; set; }

        public string T_Collectif { get; set; }

        public string T_Devise { get; set; }

        public string T_Emailing { get; set; }

        public string T_Devisemnt { get; set; }

        public string Cpf { get; set; }

        public string Integrationorigin { get; set; }
    }
}
