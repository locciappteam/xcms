﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace XCMS.Data.Model.Integration
{
    [Table("IN_CUSTOMER_INTERFACE")]
    public class IN_CUSTOMER_INTERFACE : DefaultEntity
    {
        public long ID { get; set; }

        public int? InterfaceExecutionID { get; set; }

        public int? LINE { get; set; }

        public string TYPE { get; set; }

        public string VIP_CODE { get; set; }

        public string STORECODE { get; set; }

        public string TITLE { get; set; }

        public string LAST_NAME { get; set; }

        public string FIRST_NAME { get; set; }

        public string ADDRESS1 { get; set; }

        public string ADDRESS2 { get; set; }

        public string ADDRESS3 { get; set; }

        public string CITY { get; set; }

        public string STATE { get; set; }

        public string POST_CODE { get; set; }

        public string COUNTRY { get; set; }

        public string PHONE { get; set; }

        public string MOBILE { get; set; }

        public string PHONE3 { get; set; }

        public string EMAIL { get; set; }

        public string DAY_BIRTH { get; set; }

        public string MONTH_BIRTH { get; set; }

        public string YEAR_BIRTH { get; set; }

        public string SEX { get; set; }

        public string T_ETABLISSEMENT { get; set; }

        public string T_LANGUE { get; set; }

        public string T_AUXILIAIRE { get; set; }

        public string YTC_CODECPFCNPJ { get; set; }

        public string T_NATUREAUXI { get; set; }

        public string T_DEVISE { get; set; }

        public string T_EMAILING { get; set; }

    }
}
