﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace XCMS.Data.Model.Integration
{
    [Table("IN_PAYMENT_INTERFACE")]
    public class IN_PAYMENT_INTERFACE : DefaultEntity
    {
        public Int64 ID { get; set; }

        public int? InterfaceExecutionID { get; set; }

        public int? Line { get; set; }

        public string Record_type { get; set; }

        public string Internal_Reference { get; set; }

        public string Numero { get; set; }

        public string PaymentCode { get; set; }

        public string Amount { get; set; }

        public string Currency { get; set; }
    }
}
