﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace XCMS.Data.Model.Integration
{
    [Table("EXV_STATUS_PEDIDOS")]
    public class EXV_STATUS_PEDIDO : DefaultEntity
    {
        public string PEDIDO_EXTERNO { get; set; }
        public string TIPO_PEDIDO { get; set; }
        public string TIPO_ENTREGA { get; set; }
        public string CLIENTE { get; set; }
        public string ARMAZEM { get; set; }

        public virtual ICollection<EXV_DETALHE_PEDIDO_STATUS> EXV_DETALHE_PEDIDO_STATUS { get;set;}
    }
}
