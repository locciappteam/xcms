﻿using System.ComponentModel.DataAnnotations.Schema;

namespace XCMS.Data.Model.Integration
{
    [Table("SFS_TMP_PAYMENTS")]
    public class SFS_TMP_PAYMENT : DefaultEntity
    {
        public string RecordType { get; set; }

        public string InternalReference { get; set; }

        public string Numero { get; set; }

        public string PaymentCode { get; set; }

        public string Amount { get; set; }

        public string Currency { get; set; }

        public string IntegrationOrigin { get; set; }
    }
}
