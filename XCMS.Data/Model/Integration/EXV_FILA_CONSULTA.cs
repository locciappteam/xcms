﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace XCMS.Data.Model.Integration
{
    [Table("EXV_FILA_CONSULTA")]
    public class EXV_FILA_CONSULTA : DefaultEntity
    {
        public string CODIGO { get; set; }
        public bool? CONSULTADO { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }
        public string ALTERADO_POR { get; set; }

        public string DESC_ERRO { get; set; }
    }
}
