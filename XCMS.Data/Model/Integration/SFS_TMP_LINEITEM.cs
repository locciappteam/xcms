﻿using System.ComponentModel.DataAnnotations.Schema;

namespace XCMS.Data.Model.Integration
{
    [Table("SFS_TMP_LINEITEMS")]
    public class SFS_TMP_LINEITEM : DefaultEntity
    {
        public string RecordType { get; set; }

        public string Store { get; set; }

        public string Cais { get; set; }

        public string OrderNumber { get; set; }

        public string Date { get; set; }

        public string Client { get; set; }

        public string Currency { get; set; }

        public string ItemNumber { get; set; }

        public string QuantitySecomOrderLined { get; set; }

        public string UnitPrice { get; set; }

        public string TotalPriceLine { get; set; }

        public string TotalPriceDiscountedLine { get; set; }

        public string MarkdownReason { get; set; }

        public string FooterDiscount { get; set; }

        public string TaxSystem { get; set; }

        public string Carrier { get; set; }

        public string IsGift { get; set; }

        public string Volume { get; set; }

        public string ZipCode { get; set; }

        public string IntegrationOrigin { get; set; }

    }
}
