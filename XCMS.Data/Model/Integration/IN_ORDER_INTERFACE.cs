﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace XCMS.Data.Model.Integration
{
    [Table("IN_ORDER_INTERFACE")]
    public class IN_ORDER_INTERFACE : DefaultEntity
    {
        public Int64 ID { get; set; }

        public int? InterfaceExecutionID { get; set; }

        public int? Line { get; set; }

        public string Record_type { get; set; }

        public string Store { get; set; }

        public string Cais { get; set; }

        public string Order_number { get; set; }

        public string Date { get; set; }

        public string Client { get; set; }

        public string Currency { get; set; }

        public string ItemNumber { get; set; }

        public string Quantity_Secom_Order_Lined { get; set; }

        public string Unit_Price { get; set; }

        public string Total_Price_line { get; set; }

        public string Total_Price_discounted_line { get; set; }

        public string Markdown_reason { get; set; }

        public string Footer_discount { get; set; }

        public string Tax_System { get; set; }

        public string ZipCode { get; set; }

        public string Carrier { get; set; }

        public string IsGift { get; set; }

        public string Volume { get; set; }
    }
}
