﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;

namespace XCMS.Data.Model.Json.Get
{
    public partial class StatusPedido
    {
        [JsonProperty("HEADER")]
        public List<Header> Header { get; set; }
    }

    public partial class Header
    {
        [JsonProperty("TIPO_PEDIDO")]
        public string TipoPedido { get; set; }
        [JsonProperty("TIPO_ENTREGA")]
        public object TipoEntrega { get; set; }
        [JsonProperty("CLIENTE")]
        public string Cliente { get; set; }
        [JsonProperty("ARMAZEM")]
        public string Armazem { get; set; }
        [JsonProperty("DETALHE_PEDIDO")]
        public DetalhePedido DetalhePedido { get; set; }
    }

    public partial class DetalhePedido
    {
        [JsonProperty("PEDIDO_EXTERNO")]
        public long PedidoExterno { get; set; }
        
        [JsonProperty("PEDIDO_WMS")]
        public long PedidoWms { get; set; }

        [JsonProperty("DETALHE_STATUS")]
        public List<DetalheStatus> DetalheStatus { get; set; }
    }

    public partial class DetalheStatus
    {
        [JsonProperty("STATUS")]
        public string Status { get; set; }
        [JsonProperty("STATUS_DESCRICAO")]
        public string StatusDescricao { get; set; }
        [JsonProperty("DATA")]
        public string Data { get; set; }
    }

    public class ResultadoExView
    {
        public string pedidoExterno { get; set; }
        public string pedidoWMS { get; set; }
        public string tipoEntrega { get; set; }
    }
}
