﻿using System.ComponentModel.DataAnnotations.Schema;

namespace XCMS.Data.Model.OCMS
{
    [Table("tmpOrders")]
    public class tmpOrder : DefaultEntity
    {
        public string Record_type { get; set; }

        public string Store { get; set; }

        public string Cais { get; set; }

        public string order_number { get; set; }

        public string Date { get; set; }

        public string Client { get; set; }

        public string currency { get; set; }

        public string ItemNumber { get; set; }

        public string Quantity_Secom_Order_Lined { get; set; }

        public decimal? Unit_Price { get; set; }

        public decimal? Total_Price_line { get; set; }

        public decimal? Total_Price_discounted_line { get; set; }

        public string Markdown_reason { get; set; }

        public decimal? Footer_discount { get; set; }

        public string Tax_System { get; set; }

        public string zipCode { get; set; }
    }
}
