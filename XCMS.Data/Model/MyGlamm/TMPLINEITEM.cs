﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace XCMS.Data.Model.MyGlamm
{
    [Table("tmpLineItems")]
    public class TMPLINEITEM : DefaultEntity
    {
        public string RecordType { get; set; }

        public string Store { get; set; }

        public string Cais { get; set; }

        public string order_number { get; set; }

        public string Date { get; set; }

        public string Client { get; set; }

        public string Currency { get; set; }

        public string ItemNumber { get; set; }

        public string QuantitySecomOrderLined { get; set; }

        public string Unit_Price { get; set; }

        public string Total_Price_Line { get; set; }

        public string Total_Price_discounted_line { get; set; }

        public string Markdown_reason { get; set; }

        public string Footer_discount { get; set; }

        public string Tax_System { get; set; }

        public string Carrier { get; set; }

        public string IsGift { get; set; }

        public string Volume { get; set; }
    }
}
