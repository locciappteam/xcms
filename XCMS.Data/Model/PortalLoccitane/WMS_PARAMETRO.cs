﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace XCMS.Data.Model.PortalLoccitane
{
    [Table("WMS_PARAMETROS")]
    public class WMS_PARAMETRO : DefaultEntity
    {
        public WMS_PARAMETRO()
        {
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key]
        public string PARAMETRO { get; set; }
        public string DESC_PARAMETRO { get; set; }
        public string VALOR { get; set; }
        public string TIPO { get; set; }
        public bool ATIVO { get; set; }
        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }
    }
}
