﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using XCMS.Data.Model.Integration;
using XCMS.Data.Model.MyGlamm;
using XCMS.Data.Model.PortalLoccitane;

namespace XCMS.Data.Context
{
    public class ContextDatabase : DbContext
    {
        // DB_INTERFACES (INTEGRAÇÃO)
        public DbSet<XCMS_INTEGRATED_ORDER> xcmsIntegratedOrders { get; set; }
        public DbSet<XCMS_INTEGRATED_ORDER_TRACKING> xcmsIntegratedOrdersTracking { get; set; }
        public DbSet<IN_ORDER_INTERFACE> inOrderInterface { get; set; }
        public DbSet<IN_PAYMENT_INTERFACE> inPaymentInterface { get; set; }
        public DbSet<IN_CUSTOMER_INTERFACE> inCustomerInterface { get; set; }
        public DbSet<SFS_TMP_LINEITEM> sfsTmpLineItems { get; set; }
        public DbSet<SFS_TMP_PAYMENT> sfsTmpPayments { get; set; }
        public DbSet<SFS_TMP_CUSTOMER> sfsTmpCustomers { get; set; }
        public DbSet<SFS_TMP_PIECEADRESSE> sfsTmpPieceAdresse { get; set; }
        public DbSet<EXV_STATUS_PEDIDO> exvStatusPedidos { get; set; }
        public DbSet<EXV_FILA_CONSULTA> exvFilaConsulta { get; set; }
        public DbSet<EXV_DETALHE_PEDIDO_STATUS> exvDetalhePedidoStatus { get; set; }


        // MYGLAMM
        public DbSet<TMPLINEITEM> myGlammTmpLineItems { get; set; }
  
        //OCMS
        public DbSet<Model.OCMS.tmpOrder> ocmsTmpOrders { get; set; }

        // OBMCS
        public DbSet<Model.OBCMS.tmpOrder> obcmsTmpOrders { get; set; }

        // PORTAL LOCCITANE 
        public DbSet<WMS_PARAMETRO> wmsParametros { get; set; }
        private string _connStr { get; set; } 
        public ContextDatabase(string dbMode = "INTERFACES")
        {

            // INTERFACES => Conexao_DB_INTERFACES
            // MYGLAMM => Conexao_DB_MYGLAMM
            // OCMS => Conexao_DB_OCMS
            // OBCMS => Conexao_DB_OBCMS
            // PORTALLOCCITANE => Conexao_DB_PORTALLOCITANE

            switch (dbMode)
            {
                case "INTERFACES":
                    _connStr = "Conexao_DB_INTERFACES";
                    break;

                case "OCMS":
                    _connStr = "Conexao_DB_OCMS";
                    break;

                case "OBCMS":
                    _connStr = "Conexao_DB_OBCMS";
                    break;

                case "MYGLAMM":
                  _connStr = "Conexao_DB_MYGLAMM";
                    break;

                case "PORTALLOCCITANE":
                    _connStr = "Conexao_DB_PORTALLOCCITANE";
                    break;
            }
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
           .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
           .AddJsonFile("appsettings.json")
           .Build();

            optionsBuilder.UseSqlServer(configuration.GetConnectionString(_connStr));
            
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {

            // DB_INTERFACES (INTEGRACAO)
            builder.Entity<SFS_TMP_LINEITEM>().HasNoKey();
            builder.Entity<SFS_TMP_PAYMENT>().HasNoKey();
            builder.Entity<SFS_TMP_CUSTOMER>().HasNoKey();
            builder.Entity<SFS_TMP_PIECEADRESSE>().HasNoKey();

            builder.Entity<EXV_STATUS_PEDIDO>().HasKey(p => p.PEDIDO_EXTERNO);
            builder.Entity<EXV_DETALHE_PEDIDO_STATUS>().HasKey(p => new { p.ID, p.PEDIDO_EXTERNO});
            builder.Entity<EXV_DETALHE_PEDIDO_STATUS>().Property(p => p.ID).ValueGeneratedOnAdd();

            builder.Entity<EXV_STATUS_PEDIDO>().HasMany(p=>p.EXV_DETALHE_PEDIDO_STATUS);
 
            builder.Entity<EXV_FILA_CONSULTA>().HasKey(p => p.CODIGO);


            // MYGLAMM
            builder.Entity<TMPLINEITEM>().HasNoKey();

            //OCMS
            builder.Entity<Model.OCMS.tmpOrder>().HasNoKey();

            //OBCMS
            builder.Entity<Model.OBCMS.tmpOrder>().HasNoKey();

            //PORTAL LOCCITANE

        }

    }
}
