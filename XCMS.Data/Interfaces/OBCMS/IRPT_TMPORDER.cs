﻿using XCMS.Data.Model.OBCMS;
using XCMS.Data.Repository.Interfaces;

namespace XCMS.Data.Interfaces.OBCMS
{
    public interface IRPT_TMPORDER : IRepository<tmpOrder>
    {}
}
