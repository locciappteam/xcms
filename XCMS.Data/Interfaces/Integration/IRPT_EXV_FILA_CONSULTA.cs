﻿using XCMS.Data.Model.Integration;
using XCMS.Data.Repository.Interfaces;

namespace XCMS.Data.Interfaces.Integration
{
    public interface IRPT_EXV_FILA_CONSULTA : IRepository<EXV_FILA_CONSULTA>
    {
    }
}
