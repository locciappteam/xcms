﻿using System;
using System.Collections.Generic;
using System.Text;
using XCMS.Data.Model.Integration;

namespace XCMS.Data.Repository.Interfaces.Integration
{
    public interface IRPTIN_PAYMENT_INTERFACE : IRepository<IN_PAYMENT_INTERFACE>
    {
    }
}
