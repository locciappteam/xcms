﻿using System;
using System.Collections.Generic;
using System.Text;
using XCMS.Data.Model.Integration;

namespace XCMS.Data.Repository.Interfaces.Integration
{
    public interface IRPTIN_CUSTOMER_INTERFACE : IRepository<IN_CUSTOMER_INTERFACE>
    {}
}
