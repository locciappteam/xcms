﻿using XCMS.Data.Model.Integration;
using XCMS.Data.Repository.Interfaces;

namespace XCMS.Data.Interfaces.Integration
{
    public interface IRPT_EXV_DETALHE_PEDIDO_STATUS : IRepository<EXV_DETALHE_PEDIDO_STATUS>
    {
    }
}
