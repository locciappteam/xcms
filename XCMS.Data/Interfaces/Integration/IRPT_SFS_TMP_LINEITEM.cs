﻿using XCMS.Data.Model.Integration;

namespace XCMS.Data.Repository.Interfaces.Integration
{
    public interface IRPT_SFS_TMP_LINEITEM : IRepository<SFS_TMP_LINEITEM>
    {}
}
