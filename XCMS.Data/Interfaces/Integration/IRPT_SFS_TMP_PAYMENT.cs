﻿using XCMS.Data.Model.Integration;
using XCMS.Data.Repository.Interfaces;

namespace XCMS.Data.Interfaces.Integration
{
    public interface IRPT_SFS_TMP_PAYMENT : IRepository<SFS_TMP_PAYMENT>
    {
    }
}
