﻿using XCMS.Data.Model.MyGlamm;

namespace XCMS.Data.Repository.Interfaces.MyGlamm
{
    public interface IRPT_TMPLINEITEM : IRepository<TMPLINEITEM>
    {
    }
}
