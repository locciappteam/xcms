﻿using System;
using System.Collections.Generic;
using XCMS.Data.Model;

namespace XCMS.Data.Repository.Interfaces
{
    public interface IRepository <T>:IDisposable where T: DefaultEntity
    {
        void Save();
        void Rollback();
        T Add(T entity);
        IEnumerable<T> AddList(IEnumerable<T> entity);
        T Edit(T entity);
        void Delete(T entity);
        IEnumerable<T> Get();
        T GetByID(object id);
    }
}
