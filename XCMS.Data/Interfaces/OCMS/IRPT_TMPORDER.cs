﻿using XCMS.Data.Model.OCMS;
using XCMS.Data.Repository.Interfaces;

namespace XCMS.Data.Interfaces.OCMS
{
    public interface IRPT_TMPORDER : IRepository<tmpOrder>
    {}
}
