﻿using XCMS.Data.Repository.Interfaces;
using XCMS.Data.Model.PortalLoccitane;

namespace XCMS.Data.Interfaces.PortalLoccitane
{
    public interface IRPT_WMS_PARAMETROS : IRepository<WMS_PARAMETRO>
    {
    }
}
