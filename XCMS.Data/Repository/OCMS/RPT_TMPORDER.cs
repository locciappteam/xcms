﻿using XCMS.Data.Context;

namespace XCMS.Data.Repository.OCMS
{
    public class RPT_TMPORDER : Repository<Model.OCMS.tmpOrder>, Data.Interfaces.OCMS.IRPT_TMPORDER    
    {
        public RPT_TMPORDER() : base(new ContextDatabase("OCMS"))
        {}
    }
}
