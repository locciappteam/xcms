﻿using XCMS.Data.Context;

namespace XCMS.Data.Repository.OBCMS
{
    public class RPT_TMPORDER : Repository<Model.OBCMS.tmpOrder>, Data.Interfaces.OBCMS.IRPT_TMPORDER
    {
        public RPT_TMPORDER() : base(new ContextDatabase("OBCMS"))
        {}
    }
}
