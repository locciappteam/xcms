﻿using XCMS.Data.Context;
using XCMS.Data.Interfaces.PortalLoccitane;
using XCMS.Data.Model.PortalLoccitane;

namespace XCMS.Data.Repository.PortalLoccitane
{
    public class RPT_WMS_PARAMETRO : Repository<WMS_PARAMETRO>, IRPT_WMS_PARAMETROS
    {
        public RPT_WMS_PARAMETRO() : base(new ContextDatabase("PORTALLOCCITANE")) 
        { }
    }
}
