﻿using XCMS.Data.Context;
using XCMS.Data.Model.MyGlamm;
using XCMS.Data.Repository.Interfaces.MyGlamm;

namespace XCMS.Data.Repository.MyGlamm
{
    public class RPT_TMPLINEITEM : Repository<TMPLINEITEM>, IRPT_TMPLINEITEM
    {
        public RPT_TMPLINEITEM():base(new ContextDatabase("MYGLAMM"))
        {}
    }
}
