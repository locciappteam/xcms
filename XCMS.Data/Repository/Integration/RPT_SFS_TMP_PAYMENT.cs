﻿using XCMS.Data.Context;
using XCMS.Data.Interfaces.Integration;
using XCMS.Data.Model.Integration;

namespace XCMS.Data.Repository.Integration
{
    public class RPT_SFS_TMP_PAYMENT : Repository<SFS_TMP_PAYMENT>, IRPT_SFS_TMP_PAYMENT
    {
        public RPT_SFS_TMP_PAYMENT() : base(new ContextDatabase("INTERFACES"))
        {}
    }
}
