﻿using XCMS.Data.Context;
using XCMS.Data.Model.Integration;
using XCMS.Data.Repository.Interfaces.Integration;

namespace XCMS.Data.Repository.Integration
{
    public class RPT_IN_ORDER_INTERFACE : Repository<IN_ORDER_INTERFACE>, IRPTIN_ORDER_INTERFACE
    {
        public RPT_IN_ORDER_INTERFACE() : base(new ContextDatabase("INTERFACES"))
        { }
    }
}
