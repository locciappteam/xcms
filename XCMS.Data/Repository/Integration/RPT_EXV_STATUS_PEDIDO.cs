﻿using XCMS.Data.Context;
using XCMS.Data.Interfaces.Integration;
using XCMS.Data.Model.Integration;

namespace XCMS.Data.Repository.Integration
{
    public class RPT_EXV_STATUS_PEDIDO : Repository<EXV_STATUS_PEDIDO>, IRPT_EXV_STATUS_PEDIDO
    {
        public RPT_EXV_STATUS_PEDIDO() : base(new ContextDatabase("INTERFACES"))
        { }
    }
}
