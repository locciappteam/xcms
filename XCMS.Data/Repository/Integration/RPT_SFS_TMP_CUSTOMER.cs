﻿using XCMS.Data.Context;
using XCMS.Data.Interfaces.Integration;
using XCMS.Data.Model.Integration;

namespace XCMS.Data.Repository.Integration
{
    public class RPT_SFS_TMP_CUSTOMER : Repository<SFS_TMP_CUSTOMER>, IRPT_SFS_TMP_CUSTOMER
    {   
        public RPT_SFS_TMP_CUSTOMER() : base(new ContextDatabase("INTERFACES"))
        {

        }
    }
}
