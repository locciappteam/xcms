﻿using XCMS.Data.Context;
using XCMS.Data.Model.Integration;
using XCMS.Data.Repository.Interfaces.Integration;

namespace XCMS.Data.Repository.Integration
{

    public class RPT_IN_PAYMENT_INTERFACE: Repository<IN_PAYMENT_INTERFACE>, IRPTIN_PAYMENT_INTERFACE
    {
        public RPT_IN_PAYMENT_INTERFACE() : base(new ContextDatabase("INTERFACES"))
        { }
    }
}
