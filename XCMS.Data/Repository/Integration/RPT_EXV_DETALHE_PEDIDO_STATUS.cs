﻿using XCMS.Data.Context;
using XCMS.Data.Interfaces.Integration;
using XCMS.Data.Model.Integration;

namespace XCMS.Data.Repository.Integration
{
    public class RPT_EXV_DETALHE_PEDIDO_STATUS : Repository<EXV_DETALHE_PEDIDO_STATUS>, IRPT_EXV_DETALHE_PEDIDO_STATUS
    {
        public RPT_EXV_DETALHE_PEDIDO_STATUS() : base(new ContextDatabase("INTERFACES"))
        { }
    }
}
