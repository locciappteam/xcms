﻿using XCMS.Data.Context;
using XCMS.Data.Interfaces.Integration;
using XCMS.Data.Model.Integration;

namespace XCMS.Data.Repository.Integration
{
    public class RPT_EXV_FILA_CONSULTA : Repository<EXV_FILA_CONSULTA>, IRPT_EXV_FILA_CONSULTA
    {
        public RPT_EXV_FILA_CONSULTA() : base(new ContextDatabase("INTERFACES"))
        { }
    }
}
