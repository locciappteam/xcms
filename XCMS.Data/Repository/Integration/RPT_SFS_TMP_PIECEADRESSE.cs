﻿using XCMS.Data.Context;
using XCMS.Data.Interfaces.Integration;
using XCMS.Data.Model.Integration;

namespace XCMS.Data.Repository.Integration
{
    public class RPT_SFS_TMP_PIECEADRESSE : Repository<SFS_TMP_PIECEADRESSE>, IRPT_SFS_TMP_PIECEADRESSE
    {
        public RPT_SFS_TMP_PIECEADRESSE() : base(new ContextDatabase("INTERFACES"))
        {}
    }
}
