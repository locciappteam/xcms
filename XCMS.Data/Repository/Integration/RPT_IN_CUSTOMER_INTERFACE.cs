﻿using XCMS.Data.Context;
using XCMS.Data.Model.Integration;
using XCMS.Data.Repository.Interfaces.Integration;

namespace XCMS.Data.Repository.Integration
{
    public class RPT_IN_CUSTOMER_INTERFACE: Repository<IN_CUSTOMER_INTERFACE>, IRPTIN_CUSTOMER_INTERFACE
    {
        public RPT_IN_CUSTOMER_INTERFACE():base(new ContextDatabase("INTERFACES"))
        {}
    }
}
