﻿using XCMS.Data.Context;
using XCMS.Data.Model.Integration;
using XCMS.Data.Repository.Interfaces.Integration;

namespace XCMS.Data.Repository.Integration
{
    public class RPT_SFS_TMP_LINEITEM : Repository<SFS_TMP_LINEITEM>, IRPT_SFS_TMP_LINEITEM
    {
        public RPT_SFS_TMP_LINEITEM() : base(new ContextDatabase("INTERFACES"))
        { }
    }
}
