﻿using System;
using System.Text;
using System.Threading.Tasks;
using XCMS.Data.Context;
using XCMS.Core.Import;
using System.Globalization;
using XCMS.Core;

namespace Exview.App
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("== INICIO DA CONSULTA API EXVIEW ==");
                Log.Write(String.Format($"{DateTime.Now} : \n == INICIO DA CONSULTA API EXVIEW == \n "));

                string User;
                string Pass;
                Uri baseAddress;


                using (ContextDatabase _ctx = new ContextDatabase("PORTALLOCCITANE"))
                {
                    User = _ctx.wmsParametros.Find("USUARIO_API").VALOR;
                    Pass = _ctx.wmsParametros.Find("SENHA_API").VALOR;
                    baseAddress = new Uri(_ctx.wmsParametros.Find("BASE_ADDRESS").VALOR);
                }

                byte[] byteAuth = Encoding.ASCII.GetBytes($"{User}:{Pass}");
                string Auth = Convert.ToBase64String(byteAuth);

                Task.Run(async () =>
                {                                    
                 await ConsultaExView.StatusPedidoAsync(baseAddress, Auth);                   
                }).Wait();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Erro ao Executar: " + ex.Message);
                Log.Write(String.Format($"{DateTime.Now} : \n Erro ao Executar consulta:" + ex.Message  + "\n"));
            }
            finally{
                Console.WriteLine("== FIM  DA CONSULTA API EXVIEW ==");
                Log.Write(String.Format($"{DateTime.Now} : \n == FIM  DA CONSULTA API EXVIEW == \n "));
                Environment.Exit(0);
            }
        }
    }
 }