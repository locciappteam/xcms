﻿using System;
using System.Threading.Tasks;
using XCMS.Core;
using XCMS.Core.FTP;
using XCMS.Core.Import;


namespace XCMS.App
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            string dirLocalOCMS = @"\\ambrafsep021.am.loi.net\Usuarios\TI\0 - Publico\ORDERS_XCMS\";
            string dirBackupOCMS = @"\\ambrafsep021.am.loi.net\Usuarios\TI\0 - Publico\ORDERS_XCMS\BACKUP\OCMS\";

            string dirLocalOBCMS = @"\\ambrafsep021.am.loi.net\Usuarios\TI\0 - Publico\ORDERS_XCMS\";
            string dirBackupOBCMS = @"\\ambrafsep021.am.loi.net\Usuarios\TI\0 - Publico\ORDERS_XCMS\BACKUP\OBCMS\";

            Task.Run(async () =>
            {
                Console.WriteLine("== INÍCIO DA IMPORTAÇÃO ==");
                Log.Write(String.Format($"{DateTime.Now} : \n === INÍCIO DA EXECUÇÃO === \n "));

                Console.WriteLine(".. Importando OCMS ORDERS");
                await Import.OrdersProvenceCSV("OCMS_Orders_", dirLocalOCMS, dirBackupOCMS);

                Console.WriteLine(".. Importando OCMS ORDERS TRACKING");
                await Import.OrdersTrackingProvenceCSV("OCMS_OrdersTracking_", dirLocalOCMS, dirBackupOCMS);

                Console.WriteLine(".. Importando OBCMS ORDERS");
                await Import.OrdersBresilCSV("OBCMS_Orders_", dirLocalOBCMS, dirBackupOBCMS);

                Console.WriteLine(".. Importando OBCMS ORDERS TRACKING");
                await Import.OrdersTrackingBresilCSV("OBCMS_OrdersTracking_", dirLocalOBCMS, dirBackupOBCMS);

                Console.WriteLine("== FIM DA IMPORTAÇÃO: VERIFIQUE O LOG PARA VER OS DETALHES == ");
                Log.Write(String.Format($"{DateTime.Now} : \n === FIM DA EXECUÇÃO === \n "));

                Console.ReadLine();

            }).Wait();
 
            }

            */

            string ftpHost = "ftp-retail-eu.loccitane.com";
            int ftpPort = 21;
            string ftpUser = "dataexchange";
            string ftpPass = "FTP01_Usr#Data-exchange";
            //string ftpPath = "/DataExchanger/InterfaceWeb/CEGID/OCMS_BR/Incomming/";
            string ftpPath = "/DataExchanger/InterfaceWeb/CEGID/OCMS_BR/Incomming/";

            //string LocalPath = @"\\amsaocts01\interfaces\Import\Brazil\Web_BackOffice\";
            string LocalPath = @"C:\Web_BackOffice\";

            //ConexaoFTP conn = new ConexaoFTP(ftpHost, ftpPort, ftpUser, ftpPass, ftpPath, LocalPath);

            //conn.DownloadFiles(LocalPath);
            //conn.DownloadFiles(LocalPath, "ExportOrder*.csv");
            Task.Run(async () =>
            {
                await Integration.ImportLineItemsFromCSVAsync(@"C:\Web_BackOffice\OAB_IN\", @"C:\Web_BackOffice\OAB_IN\log\");
                await Integration.ImportPaymentsFromCSV(@"C:\Web_BackOffice\OAB_IN\", @"C:\Web_BackOffice\OAB_IN\log\");
                await Integration.ImportCustomersFromCSV(@"C:\Web_BackOffice\OAB_IN\", @"C:\Web_BackOffice\OAB_IN\log\");

            }).Wait();
            
        }
    }
}
