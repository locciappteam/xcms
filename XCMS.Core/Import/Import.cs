﻿using EFCore.BulkExtensions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XCMS.Data.Context;
using XCMS.Data.Model.Integration;
using XCMS.Data.Model.Integration.Excel;

namespace XCMS.Core.Import
{
    public class Import
    {

        private static CultureInfo culture = new CultureInfo("en-US");

        public static async Task OrdersProvenceCSV(string fileNomenclature, string sourcePath, string backupPath)
        {

            Log.Write(String.Format($"{DateTime.Now} : \n INÍCIO Importação Orders OCMS \n "));
            string[] fileList = Directory.GetFiles(sourcePath, string.Format("{0}*.csv", fileNomenclature));

            Log.Write(String.Format($"{DateTime.Now} : \n Encontrado(s) {fileList.Count()} Arquivo(s) \n "));

            if (fileList.Any())
            {
                Log.Write(String.Format($"{DateTime.Now} : \n Início da formatação dos registros do Arquivo \n "));

                foreach (var file in fileList)
                {
                    var filename = file.Substring(sourcePath.Length);
                    string[] contentFile = File.ReadLines(file, Encoding.GetEncoding("iso-8859-1")).Skip(1).ToArray();
                    List<OrderSQLQuery> ListaOrderSQLQuery = new List<OrderSQLQuery>();

                    foreach (var line in contentFile)
                    {
                        try
                        {
                            OrderSQLQuery _OrderSQLQuery = new OrderSQLQuery
                            {
                                Order               = Convert.ToInt64(line.Split(';')[0]),
                                Customer            = Convert.ToInt64(line.Split(';')[1]),
                                Email               = line.Split(';')[2],
                                FirstName           = line.Split(';')[3],
                                LastName            = line.Split(';')[4],
                                NbLine              = Convert.ToInt16(line.Split(';')[5]),
                                OrderTotal          = (line.Split(';')[6] == "") ? 0 : Convert.ToDecimal(line.Split(';')[6].Replace(".", ",")),
                                ShippingFee         = (line.Split(';')[7] == "") ? 0 : Convert.ToDecimal(line.Split(';')[7].Replace(".", ",")),
                                Tax                 = (line.Split(';')[8] == "") ? 0 : Convert.ToDecimal(line.Split(';')[8].Replace(".", ",")),
                                GlobalTotal         = (line.Split(';')[9] == "") ? 0 : Convert.ToDecimal(line.Split(';')[9].Replace(".", ",")),
                                Status              = line.Split(';')[10],
                                Comment             = line.Split(';')[11],
                                DateCreated         = (line.Split(';')[12] == "") ? DateTime.MinValue : DateTime.Parse(line.Split(';')[12], new CultureInfo("pt-BR")),
                                Info                = line.Split(';')[13],
                                Tracking            = line.Split(';')[14],
                                ShippingMethod      = line.Split(';')[15],
                                PaymentMethod       = line.Split(';')[16],
                                AuthNumber = "",
                                TransactionID = "",
                                //AuthNumber          = line.Split(';')[17] != "" ? long.Parse(line.Split(';')[17], NumberStyles.Float).ToString() : "",
                                //TransactionID       = line.Split(';')[18] != "" ? long.Parse(line.Split(';')[18], NumberStyles.Float).ToString() : "",
                                CardType = line.Split(';')[19],
                                LastModifiedBy      = line.Split(';')[20],
                                DateModified        = (line.Split(';')[21] == "") ? DateTime.MinValue : DateTime.Parse(line.Split(';')[21], new CultureInfo("pt-BR")),
                                DiscountTotal       = (line.Split(';')[22] == "") ? 0 : Convert.ToDecimal(line.Split(';')[22].Replace(".", ",")),
                                ShippingFirstName   = line.Split(';')[23],
                                ShippingLastName    = line.Split(';')[24],
                                ShippingPhone       = line.Split(';')[25],
                                ShippingZip         = line.Split(';')[26],
                                ShippingCity        = line.Split(';')[27],
                                ShippingState       = line.Split(';')[28],
                                CustomerOptional1   = line.Split(';')[29],
                                CustomerOptional2   = line.Split(';')[30],
                                CustomerOptional3   = line.Split(';')[31],
                                CustomerOptional4   = line.Split(';')[32],
                                CustomerOptional5   = line.Split(';')[33],
                                CustomerAddress     = line.Split(';')[34],
                                CustomerAddress2    = line.Split(';')[35],
                                CustomerAddress3    = line.Split(';')[36],
                                CustomerAddress4    = line.Split(';')[37],
                                CustomerAddress5    = line.Split(';')[38],
                                CustomerAddress6    = line.Split(';')[39],
                                CustomerAddress7    = line.Split(';')[40],
                                CustomerAddress8    = line.Split(';')[41],
                                CustomerAddress9    = line.Split(';')[42],
                                ShippingAddress     = line.Split(';')[43],
                                ShippingAddress2    = line.Split(';')[44],
                                ShippingAddress3    = line.Split(';')[45],
                                ShippingAddress4    = line.Split(';')[46],
                                ShippingAddress5    = line.Split(';')[47],
                                ShippingAddress6    = line.Split(';')[48],
                                ShippingAddress7    = line.Split(';')[49],
                                ShippingAddress8    = line.Split(';')[50],
                                ShippingAddress9    = line.Split(';')[51],
                                Source              = line.Split(';')[52],
                                GiftMessage         = line.Split(';')[53],
                                GiftInstructions    = line.Split(';')[54]
                            };
                            ListaOrderSQLQuery.Add(_OrderSQLQuery);

                        }
                        catch (Exception ex)
                        {
                            Log.Write(String.Format($"{DateTime.Now} : \n Erro ao processar linha : {line.Split(';')[0]} \n Erro: {ex.InnerException}"));
                            continue;
                        }
                    }
                    Log.Write(String.Format($"{DateTime.Now} : \n Fim da formatação dos registros do Arquivo : {filename} \n"));

                    List<XCMS_INTEGRATED_ORDER> lista = new List<XCMS_INTEGRATED_ORDER>();
                    using (var _ctx = new ContextDatabase())
                    {
                        Log.Write(String.Format($"{DateTime.Now} : \n Início da importação do arquivo : {filename} \n"));

                        lista = (
                                    from tbl in ListaOrderSQLQuery.Distinct()
                                    select new XCMS_INTEGRATED_ORDER
                                    {
                                        ORDERID = tbl.Order
                                        ,MARCAID = 2 // 1 BRESIL / 2 PROVENCE
                                        ,CUSTOMERID = tbl.Customer
                                        ,EMAIL = tbl.Email
                                        ,FIRSTNAME = tbl.FirstName
                                        ,LASTNAME = tbl.LastName
                                        ,NBLINE = tbl.NbLine
                                        ,ORDERTOTAL = tbl.OrderTotal
                                        ,SHIPPINGFEE = tbl.ShippingFee
                                        ,TAX = tbl.Tax
                                        ,GLOBALTOTAL = tbl.GlobalTotal
                                        ,STATUS = tbl.Status
                                        ,COMMENT = tbl.Comment
                                        ,DATECREATED = tbl.DateCreated
                                        ,INFO = tbl.Info
                                        ,TRACKING = tbl.Tracking
                                        ,SHIPPINGMETHOD = tbl.ShippingMethod
                                        ,PAYMENTMETHOD = tbl.PaymentMethod
                                        ,AUTHNUMBER = tbl.AuthNumber
                                        ,TRANSACTIONID = tbl.TransactionID
                                        ,CARDTYPE = tbl.CardType
                                        ,LASTMODIFIEDBY = tbl.LastModifiedBy
                                        ,DATEMODIFIED = tbl.DateModified
                                        ,DISCOUNTTOTAL = tbl.DiscountTotal
                                        ,SHIPPINGFIRSTNAME = tbl.ShippingFirstName
                                        ,SHIPPINGLASTNAME = tbl.ShippingLastName
                                        ,SHIPPINGPHONE = tbl.ShippingPhone
                                        ,SHIPPINGZIP = tbl.ShippingZip
                                        ,SHIPPINGCITY = tbl.ShippingCity
                                        ,SHIPPINGSTATE = tbl.ShippingState
                                        ,CUSTOMEROPTIONAL1 = tbl.CustomerOptional1
                                        ,CUSTOMEROPTIONAL2 = tbl.CustomerOptional2
                                        ,CUSTOMEROPTIONAL3 = tbl.CustomerOptional3
                                        ,CUSTOMEROPTIONAL4 = tbl.CustomerOptional4
                                        ,CUSTOMEROPTIONAL5 = tbl.CustomerOptional5
                                        ,CUSTOMERADDRESS = tbl.CustomerAddress
                                        ,CUSTOMERADDRESS2 = tbl.CustomerAddress2
                                        ,CUSTOMERADDRESS3 = tbl.CustomerAddress3
                                        ,CUSTOMERADDRESS4 = tbl.CustomerAddress4
                                        ,CUSTOMERADDRESS5 = tbl.CustomerAddress5
                                        ,CUSTOMERADDRESS6 = tbl.CustomerAddress6
                                        ,CUSTOMERADDRESS7 = tbl.CustomerAddress7
                                        ,CUSTOMERADDRESS8 = tbl.CustomerAddress8
                                        ,CUSTOMERADDRESS9 = tbl.CustomerAddress9
                                        ,SHIPPINGADDRESS = tbl.ShippingAddress
                                        ,SHIPPINGADDRESS2 = tbl.ShippingAddress2
                                        ,SHIPPINGADDRESS3 = tbl.ShippingAddress3
                                        ,SHIPPINGADDRESS4 = tbl.ShippingAddress4
                                        ,SHIPPINGADDRESS5 = tbl.ShippingAddress5
                                        ,SHIPPINGADDRESS6 = tbl.ShippingAddress6
                                        ,SHIPPINGADDRESS7 = tbl.ShippingAddress7
                                        ,SHIPPINGADDRESS8 = tbl.ShippingAddress8
                                        ,SHIPPINGADDRESS9 = tbl.ShippingAddress9
                                        ,SOURCE = tbl.Source
                                        ,GIFTMESSAGE = tbl.GiftMessage
                                        ,GIFTINSTRUCTIONS = tbl.GiftInstructions
                                        ,MUPID = (tbl.Source == "MUP" && tbl.Email != "") ? tbl.Email.Substring(0, tbl.Email.IndexOf("@")) : null
                                    }
                                ).ToList();

                        using (var transaction = await _ctx.Database.BeginTransactionAsync())
                        {
                            try
                            {
                                _ctx.BulkInsertOrUpdate(lista, options => options.BatchSize = 10000);
                                await transaction.CommitAsync();
                            }
                            catch (Exception ex)
                            {
                                await transaction.RollbackAsync();
                                Log.Write(String.Format($"{DateTime.Now} : \n Erro ao importar Arquivo {filename} Erro: {ex}\n"));
                            }
                        }
                    }
                    File.Move(file, backupPath + "Success_" + DateTime.Now.ToString("yyyy_MM_dd_HHmmss")  + "_" + filename);
                    Log.Write(String.Format($"{DateTime.Now} : \n Arquivo importado com sucesso. Movido para pasta de backup!\n"));
                }
            }
            Log.Write(String.Format($"{DateTime.Now} : \n FIM Importação Orders OCMS \n "));
        }

        public static async Task OrdersBresilCSV(string fileNomenclature, string sourcePath, string backupPath)
        {
            Log.Write(String.Format($"{DateTime.Now} : \n ÍNICIO Importação Orders OBCMS \n "));
            string[] fileList = Directory.GetFiles(sourcePath, string.Format("{0}*.csv", fileNomenclature));

            Log.Write(String.Format($"{DateTime.Now} : \n Encontrado(s) {fileList.Count()} Arquivo(s) \n "));

            if (fileList.Any())
            {
                Log.Write(String.Format($"{DateTime.Now} : \n Início da formatação dos registros do Arquivo \n "));

                foreach (var file in fileList)
                {
                    var filename = file.Substring(sourcePath.Length);
                    string[] contentFile = File.ReadLines(file, Encoding.GetEncoding("iso-8859-1")).Skip(1).ToArray();
                    List<OrderSQLQuery> ListaOrderSQLQuery = new List<OrderSQLQuery>();

                    foreach (var line in contentFile)
                    {
                        try
                        {
                            OrderSQLQuery _OrderSQLQuery = new OrderSQLQuery
                            {
                                Order = Convert.ToInt64(line.Split(';')[0]),
                                Customer = Convert.ToInt64(line.Split(';')[1]),
                                Email = line.Split(';')[2],
                                FirstName = line.Split(';')[3],
                                LastName = line.Split(';')[4],
                                NbLine = Convert.ToInt16(line.Split(';')[5]),
                                OrderTotal = (line.Split(';')[6] == "") ? 0 : Convert.ToDecimal(line.Split(';')[6].Replace(".", ",")),
                                ShippingFee = (line.Split(';')[7] == "") ? 0 : Convert.ToDecimal(line.Split(';')[7].Replace(".", ",")),
                                Tax = (line.Split(';')[8] == "") ? 0 : Convert.ToDecimal(line.Split(';')[8].Replace(".", ",")),
                                GlobalTotal = (line.Split(';')[9] == "") ? 0 : Convert.ToDecimal(line.Split(';')[9].Replace(".", ",")),
                                Status = line.Split(';')[10],
                                Comment = line.Split(';')[11],
                                DateCreated = (line.Split(';')[12] == "") ? DateTime.MinValue : DateTime.Parse(line.Split(';')[12], new CultureInfo("pt-BR")),
                                Info = line.Split(';')[13],
                                Tracking = line.Split(';')[14],
                                ShippingMethod = line.Split(';')[15],
                                PaymentMethod = line.Split(';')[16],
                                AuthNumber = "",
                                TransactionID = "",
                                //AuthNumber = line.Split(';')[17] != "" ? long.Parse(line.Split(';')[17], NumberStyles.Float).ToString() : "",
                                //TransactionID = line.Split(';')[18] != "" ? long.Parse(line.Split(';')[18], NumberStyles.Float).ToString() : "",
                                CardType = line.Split(';')[19],
                                LastModifiedBy = line.Split(';')[20],
                                DateModified = (line.Split(';')[21] == "") ? DateTime.MinValue : DateTime.Parse(line.Split(';')[21], new CultureInfo("pt-BR")),
                                DiscountTotal = (line.Split(';')[22] == "") ? 0 : Convert.ToDecimal(line.Split(';')[22].Replace(".", ",")),
                                ShippingFirstName = line.Split(';')[23],
                                ShippingLastName = line.Split(';')[24],
                                ShippingPhone = line.Split(';')[25],
                                ShippingZip = line.Split(';')[26],
                                ShippingCity = line.Split(';')[27],
                                ShippingState = line.Split(';')[28],
                                CustomerOptional1 = line.Split(';')[29],
                                CustomerOptional2 = line.Split(';')[30],
                                CustomerOptional3 = line.Split(';')[31],
                                CustomerOptional4 = line.Split(';')[32],
                                CustomerOptional5 = line.Split(';')[33],
                                CustomerAddress = line.Split(';')[34],
                                CustomerAddress2 = line.Split(';')[35],
                                CustomerAddress3 = line.Split(';')[36],
                                CustomerAddress4 = line.Split(';')[37],
                                CustomerAddress5 = line.Split(';')[38],
                                CustomerAddress6 = line.Split(';')[39],
                                CustomerAddress7 = line.Split(';')[40],
                                CustomerAddress8 = line.Split(';')[41],
                                CustomerAddress9 = line.Split(';')[42],
                                ShippingAddress = line.Split(';')[43],
                                ShippingAddress2 = line.Split(';')[44],
                                ShippingAddress3 = line.Split(';')[45],
                                ShippingAddress4 = line.Split(';')[46],
                                ShippingAddress5 = line.Split(';')[47],
                                ShippingAddress6 = line.Split(';')[48],
                                ShippingAddress7 = line.Split(';')[49],
                                ShippingAddress8 = line.Split(';')[50],
                                ShippingAddress9 = line.Split(';')[51],
                                Source = line.Split(';')[52],
                                GiftMessage = line.Split(';')[53],
                                GiftInstructions = line.Split(';')[54]
                            };
                            ListaOrderSQLQuery.Add(_OrderSQLQuery);

                        }
                        catch (Exception ex)
                        {
                            Log.Write(String.Format($"{DateTime.Now} : \n Erro ao processar linha : {line.Split(';')[0]} \n Erro: {ex}"));
                            continue;
                        }
                    }
                    Log.Write(String.Format($"{DateTime.Now} : \n Fim da formatação dos registros do Arquivo : {filename} \n"));

                    List<XCMS_INTEGRATED_ORDER> lista = new List<XCMS_INTEGRATED_ORDER>();
                    using (var _ctx = new ContextDatabase())
                    {
                        Log.Write(String.Format($"{DateTime.Now} : \n Início da importação do arquivo : {filename} \n"));

                        lista = (
                                    from tbl in ListaOrderSQLQuery.Distinct()
                                    select new XCMS_INTEGRATED_ORDER
                                    {
                                        ORDERID = tbl.Order,
                                        MARCAID = 1, // 1 BRESIL / 2 PROVENCE
                                        CUSTOMERID = tbl.Customer,
                                        EMAIL = tbl.Email,
                                        FIRSTNAME = tbl.FirstName,
                                        LASTNAME = tbl.LastName,
                                        NBLINE = tbl.NbLine,
                                        ORDERTOTAL = tbl.OrderTotal,
                                        SHIPPINGFEE = tbl.ShippingFee,
                                        TAX = tbl.Tax,
                                        GLOBALTOTAL = tbl.GlobalTotal,
                                        STATUS = tbl.Status,
                                        COMMENT = tbl.Comment,
                                        DATECREATED = tbl.DateCreated,
                                        INFO = tbl.Info,
                                        TRACKING = tbl.Tracking,
                                        SHIPPINGMETHOD = tbl.ShippingMethod,
                                        PAYMENTMETHOD = tbl.PaymentMethod,
                                        AUTHNUMBER = tbl.AuthNumber,
                                        TRANSACTIONID = tbl.TransactionID,
                                        CARDTYPE = tbl.CardType,
                                        LASTMODIFIEDBY = tbl.LastModifiedBy,
                                        DATEMODIFIED = tbl.DateModified,
                                        DISCOUNTTOTAL = tbl.DiscountTotal,
                                        SHIPPINGFIRSTNAME = tbl.ShippingFirstName,
                                        SHIPPINGLASTNAME = tbl.ShippingLastName,
                                        SHIPPINGPHONE = tbl.ShippingPhone,
                                        SHIPPINGZIP = tbl.ShippingZip,
                                        SHIPPINGCITY = tbl.ShippingCity,
                                        SHIPPINGSTATE = tbl.ShippingState,
                                        CUSTOMEROPTIONAL1 = tbl.CustomerOptional1,
                                        CUSTOMEROPTIONAL2 = tbl.CustomerOptional2,
                                        CUSTOMEROPTIONAL3 = tbl.CustomerOptional3,
                                        CUSTOMEROPTIONAL4 = tbl.CustomerOptional4,
                                        CUSTOMEROPTIONAL5 = tbl.CustomerOptional5,
                                        CUSTOMERADDRESS = tbl.CustomerAddress,
                                        CUSTOMERADDRESS2 = tbl.CustomerAddress2,
                                        CUSTOMERADDRESS3 = tbl.CustomerAddress3,
                                        CUSTOMERADDRESS4 = tbl.CustomerAddress4,
                                        CUSTOMERADDRESS5 = tbl.CustomerAddress5,
                                        CUSTOMERADDRESS6 = tbl.CustomerAddress6,
                                        CUSTOMERADDRESS7 = tbl.CustomerAddress7,
                                        CUSTOMERADDRESS8 = tbl.CustomerAddress8,
                                        CUSTOMERADDRESS9 = tbl.CustomerAddress9,
                                        SHIPPINGADDRESS = tbl.ShippingAddress,
                                        SHIPPINGADDRESS2 = tbl.ShippingAddress2,
                                        SHIPPINGADDRESS3 = tbl.ShippingAddress3,
                                        SHIPPINGADDRESS4 = tbl.ShippingAddress4,
                                        SHIPPINGADDRESS5 = tbl.ShippingAddress5,
                                        SHIPPINGADDRESS6 = tbl.ShippingAddress6,
                                        SHIPPINGADDRESS7 = tbl.ShippingAddress7,
                                        SHIPPINGADDRESS8 = tbl.ShippingAddress8,
                                        SHIPPINGADDRESS9 = tbl.ShippingAddress9,
                                        SOURCE = tbl.Source,
                                        GIFTMESSAGE = tbl.GiftMessage,
                                        GIFTINSTRUCTIONS = tbl.GiftInstructions,
                                        MUPID = (tbl.Source == "MUP" && tbl.Email != "") ? tbl.Email.Substring(0, tbl.Email.IndexOf("@")) : null
                                    }
                                ).ToList();

                        using (var transaction = await _ctx.Database.BeginTransactionAsync())
                        {
                            try
                            {
                                _ctx.BulkInsertOrUpdate(lista, options => options.BatchSize = 10000);
                                await transaction.CommitAsync();
                            }
                            catch (Exception ex)
                            {
                                await transaction.RollbackAsync();
                                Log.Write(String.Format($"{DateTime.Now} : \n Erro ao importar Arquivo {filename} Erro: {ex}\n"));
                            }
                        }
                    }
                    File.Move(file, backupPath + "Success_" + DateTime.Now.ToString("yyyy_MM_dd_HHmmss") + "_" + filename);
                    Log.Write(String.Format($"{DateTime.Now} : \n Arquivo importado com sucesso. Movido para pasta de backup!\n"));
                }
            }
            Log.Write(String.Format($"{DateTime.Now} : \n FIM Importação Orders OCMS \n "));
        }

        public static async Task OrdersTrackingProvenceCSV(string fileNomenclature, string sourcePath, string backupPath)
        {
           
            Log.Write(String.Format($"{DateTime.Now} : \n INÍCIO Importação Orders Tracking OCMS \n "));
            string[] fileList = Directory.GetFiles(sourcePath, string.Format("{0}*.csv", fileNomenclature));
            Log.Write(String.Format($"{DateTime.Now} : \n Encontrado(s) {fileList.Count()} Arquivo(s) \n "));
            if (fileList.Any())
            {
                Log.Write(String.Format($"{DateTime.Now} : \n Início da formatação dos registros do Arquivo \n "));
                foreach (var file in fileList)
                {
                    List<OrderTrackingInfo> ListaOrderTrackingInfo = new List<OrderTrackingInfo>();
                    string[] fileContent = File.ReadLines(file, Encoding.GetEncoding("iso-8859-1")).Skip(1).ToArray();
                    var filename = file.Substring(sourcePath.Length);

                    foreach (var line in fileContent)
                    {
                        try
                        {

                            OrderTrackingInfo _OrderTrackingInfo = new OrderTrackingInfo
                            {
                                OrderId = Convert.ToInt64(line.Split(';')[0]),

                                CreationDate = (line.Split(';')[1] == "") ? DateTime.MinValue : DateTime.Parse(line.Split(';')[1], culture),
                                LastModification = (line.Split(';')[2] == "") ? DateTime.MinValue : DateTime.Parse(line.Split(';')[2], culture),
                                Name = line.Split(';')[3],
                                StatusFrom = line.Split(';')[4],
                                StatusTo = line.Split(';')[5],
                                OrderInfo = line.Split(';')[6]
                            };
                            ListaOrderTrackingInfo.Add(_OrderTrackingInfo);

                        }
                        catch (Exception ex)
                        {
                            Log.Write(String.Format($"{DateTime.Now} : \n Erro ao processar linha : {line.Split(';')[0]} \n Erro: {ex}"));
                            continue;
                        }
                    }

                    Log.Write(String.Format($"{DateTime.Now} : \n Fim da formatação dos registros do Arquivo : {filename} \n"));

                    List<XCMS_INTEGRATED_ORDER_TRACKING> lista = new List<XCMS_INTEGRATED_ORDER_TRACKING>();
                    using (var _ctx = new ContextDatabase())
                    {

                        Log.Write(String.Format($"{DateTime.Now} : \n Início da importação do arquivo : {filename} \n"));

                        lista = (
                                    from tbl in ListaOrderTrackingInfo
                                    select new XCMS_INTEGRATED_ORDER_TRACKING
                                    {
                                        ORDERID = tbl.OrderId
                                        ,CREATIONDATE = tbl.CreationDate
                                        ,LASTMODIFICATION = tbl.LastModification
                                        ,NAME = tbl.Name
                                        ,STATUSFROM = tbl.StatusFrom
                                        ,STATUSTO = tbl.StatusTo
                                        ,ORDERINFO = tbl.OrderInfo
                                    }
                                ).ToList();

                        using (var transaction = await _ctx.Database.BeginTransactionAsync())
                        {
                            try
                            {
                                await _ctx.BulkInsertAsync(lista, options => options.BatchSize = 10000);
                                await transaction.CommitAsync();
                            }
                            catch (Exception ex)
                            {
                                Log.Write(String.Format($"{DateTime.Now} : \n Erro ao importar Arquivo {filename} Erro: {ex}\n"));
                                await transaction.RollbackAsync();
                            }
                        }
                    }

                    File.Move(file, backupPath + "Success_" + DateTime.Now.ToString("yyyy_MM_dd_HHmmss") + "_" + filename);
                    Log.Write(String.Format($"{DateTime.Now} : \n Arquivo importado com sucesso. Movido para pasta de backup!\n"));
                }
            }
            Log.Write(String.Format($"{DateTime.Now} : \n FIM Importação Orders Tracking OCMS \n "));
        }

        public static async Task OrdersTrackingBresilCSV(string fileNomenclature, string sourcePath, string backupPath)
        {
            Log.Write(String.Format($"{DateTime.Now} : \n INÍCIO Importação Orders Tracking OCMS \n "));
            string[] fileList = Directory.GetFiles(sourcePath, string.Format("{0}*.csv", fileNomenclature));
            Log.Write(String.Format($"{DateTime.Now} : \n Encontrado(s) {fileList.Count()} Arquivo(s) \n "));
            if (fileList.Any())
            {
                Log.Write(String.Format($"{DateTime.Now} : \n Início da formatação dos registros do Arquivo \n "));
                foreach (var file in fileList)
                {
                    List<OrderTrackingInfo> ListaOrderTrackingInfo = new List<OrderTrackingInfo>();
                    string[] fileContent = File.ReadLines(file, Encoding.GetEncoding("iso-8859-1")).Skip(1).ToArray();
                    var filename = file.Substring(sourcePath.Length);

                    foreach (var line in fileContent)
                    {
                        try
                        {

                            OrderTrackingInfo _OrderTrackingInfo = new OrderTrackingInfo
                            {
                                OrderId = Convert.ToInt64(line.Split(';')[0]),

                                CreationDate = (line.Split(';')[1] == "") ? DateTime.MinValue : DateTime.Parse(line.Split(';')[1], culture),
                                LastModification = (line.Split(';')[2] == "") ? DateTime.MinValue : DateTime.Parse(line.Split(';')[2], culture),
                                Name = line.Split(';')[3],
                                StatusFrom = line.Split(';')[4],
                                StatusTo = line.Split(';')[5],
                                OrderInfo = line.Split(';')[6]
                            };
                            ListaOrderTrackingInfo.Add(_OrderTrackingInfo);

                        }
                        catch (Exception ex)
                        {
                            Log.Write(String.Format($"{DateTime.Now} : \n Erro ao processar linha : {line.Split(';')[0]} \n Erro: {ex}"));
                            continue;
                        }
                    }

                    Log.Write(String.Format($"{DateTime.Now} : \n Fim da formatação dos registros do Arquivo : {filename} \n"));

                    List<XCMS_INTEGRATED_ORDER_TRACKING> lista = new List<XCMS_INTEGRATED_ORDER_TRACKING>();
                    using (var _ctx = new ContextDatabase())
                    {

                        Log.Write(String.Format($"{DateTime.Now} : \n Início da importação do arquivo : {filename} \n"));

                        lista = (
                                    from tbl in ListaOrderTrackingInfo
                                    select new XCMS_INTEGRATED_ORDER_TRACKING
                                    {
                                        ORDERID = tbl.OrderId
                                        ,
                                        CREATIONDATE = tbl.CreationDate
                                        ,
                                        LASTMODIFICATION = tbl.LastModification
                                        ,
                                        NAME = tbl.Name
                                        ,
                                        STATUSFROM = tbl.StatusFrom
                                        ,
                                        STATUSTO = tbl.StatusTo
                                        ,
                                        ORDERINFO = tbl.OrderInfo
                                    }
                                ).ToList();

                        using (var transaction = await _ctx.Database.BeginTransactionAsync())
                        {
                            try
                            {
                                await _ctx.BulkInsertAsync(lista, options => options.BatchSize = 10000);
                                await transaction.CommitAsync();
                            }
                            catch (Exception ex)
                            {
                                Log.Write(String.Format($"{DateTime.Now} : \n Erro ao importar Arquivo {filename} Erro: {ex}\n"));
                                await transaction.RollbackAsync();
                            }
                        }
                    }

                    File.Move(file, backupPath + "Success_" + DateTime.Now.ToString("yyyy_MM_dd_HHmmss") + "_" + filename);
                    Log.Write(String.Format($"{DateTime.Now} : \n Arquivo importado com sucesso. Movido para pasta de backup!\n"));
                }
            }
            Log.Write(String.Format($"{DateTime.Now} : \n FIM Importação Orders Tracking OCMS \n "));
        }

    }
}