﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XCMS.Data.Model.Integration;
using XCMS.Data.Model.Integration.Excel;
using XCMS.Data.Repository.Integration;

namespace XCMS.Core.Import
{
    public class Integration
    {
        public static async Task ImportLineItemsFromCSVAsync(string sourcePath, string backupPath, string fileName = "ExportOrder_LineItems.csv")
        {
            FileInfo fName;
            string[] fileList = Directory.GetFiles(sourcePath, fileName);
            Log.Write(String.Format($"{DateTime.Now} : \n INÍCIO Importação Orders OCMS \n "));

            if (fileList.Any())
            {
                fName = new FileInfo(sourcePath + fileName);

                List<ExportOrderLineItem> ListLineItems = new List<ExportOrderLineItem>();

                foreach (var file in fileList)
                {

                    string[] contentFile = File.ReadLines(file, Encoding.GetEncoding("iso-8859-1")).Skip(1).ToArray();
                    foreach(var line in contentFile)
                    {
                        try
                        {
                            ExportOrderLineItem _exportOrderLineItem = new ExportOrderLineItem
                            {
                                recordType = line.Split(";")[0],
                                store = line.Split(";")[1],
                                cais = line.Split(";")[2],
                                orderNumber = line.Split(";")[3],
                                date = line.Split(";")[4],
                                client = line.Split(";")[5],
                                currency = line.Split(";")[6],
                                itemNumber = line.Split(";")[7],
                                quantitySecomOrderLined = line.Split(";")[8],
                                unitPrice = line.Split(";")[9],
                                totalPriceLine = line.Split(";")[10],
                                totalPriceDiscountedLine = line.Split(";")[11],
                                markdownReason = line.Split(";")[12],
                                footerDiscount = line.Split(";")[13],
                                taxSystem = line.Split(";")[14],
                                zipCode = line.Split(";")[15],
                                carrier = line.Split(";")[16],
                                isGift = line.Split(";")[17],
                                volume = line.Split(";")[18]
                            };

                            ListLineItems.Add(_exportOrderLineItem);
                        }
                        catch (Exception ex)
                        {
                            Log.Write(String.Format($"{DateTime.Now} : \n Erro ao processar linha : {line.Split(';')[0]} \n Erro: {ex.InnerException}"));
                            continue;
                        }
                    }
                }


                var list = (

                    from tbl in ListLineItems
                    select new IN_ORDER_INTERFACE
                    {
                        Record_type = tbl.recordType,
                        Store = tbl.store,
                        Cais = tbl.cais,
                        Order_number = tbl.orderNumber,
                        Date = tbl.date,
                        Client = tbl.client,
                        Currency = tbl.currency,
                        ItemNumber = tbl.itemNumber,
                        Quantity_Secom_Order_Lined = tbl.itemNumber,
                        Unit_Price = tbl.unitPrice,
                        Total_Price_line = tbl.unitPrice,
                        Total_Price_discounted_line = tbl.totalPriceDiscountedLine,
                        Markdown_reason = tbl.markdownReason,
                        Footer_discount = tbl.footerDiscount,
                        Tax_System = tbl.taxSystem,
                        ZipCode = tbl.zipCode,
                        Carrier = tbl.carrier,
                        IsGift = tbl.isGift,
                        Volume = tbl.volume
                    }
                    ).ToList();

                using (var _ctx = new RPT_IN_ORDER_INTERFACE())
                {

                    try
                    {
                        if(await _ctx.BulkingInsert(list))
                        {
                            Log.Write(String.Format($"{DateTime.Now} : \n LINHAS INSERIDAS COM SUCESSO \n "));
                        }
                        else
                        {
                            Log.Write(String.Format($"{DateTime.Now} : \n ERRO AO INSERIR LINHAS\n "));
                        }

                        File.Move(fName.FullName, backupPath + fName.Name.Replace(".csv", "") + string.Format("{0:_yyyyMMdd}", DateTime.Now) + ".csv");

                    }
                    catch (Exception ex)
                    {
                        Log.Write(String.Format($"{DateTime.Now} : \n ERRRO GERAL AO INSERIR LINHAS \n "));
                    }

                }

            }
        }
        public static async Task ImportPaymentsFromCSV(string sourcePath, string backupPath, string fileName = "ExportOrder_Payments.csv")
        {
            FileInfo fName;
            string[] fileList = Directory.GetFiles(sourcePath, fileName);
            Log.Write(String.Format($"{DateTime.Now} : \n INÍCIO Importação Orders OCMS \n "));

            if (fileList.Any())
            {
                fName = new FileInfo(sourcePath + fileName);

                List<ExportOrderPayment> ListPayments= new List<ExportOrderPayment>();

                foreach (var file in fileList)
                {

                    string[] contentFile = File.ReadLines(file, Encoding.GetEncoding("iso-8859-1")).Skip(1).ToArray();
                    foreach (var line in contentFile)
                    {
                        try
                        {
                            ExportOrderPayment _exportOrderPayment = new ExportOrderPayment
                            {
                                recordType = line.Split(";")[0],
                                internalReference = line.Split(";")[1],
                                numero = line.Split(";")[2],
                                paymentCode = line.Split(";")[3],
                                amount = line.Split(";")[4],
                                currency = line.Split(";")[5],
                            };

                            ListPayments.Add(_exportOrderPayment);
                        }
                        catch (Exception ex)
                        {
                            Log.Write(String.Format($"{DateTime.Now} : \n Erro ao processar linha : {line.Split(';')[0]} \n Erro: {ex.InnerException}"));
                            continue;
                        }
                    }
                }

                var list = (

                    from tbl in ListPayments
                    select new IN_PAYMENT_INTERFACE
                    {
                        Record_type = tbl.recordType,
                        Internal_Reference= tbl.internalReference,
                        Numero = tbl.numero,
                        PaymentCode = tbl.paymentCode,
                        Amount = tbl.amount,
                        Currency= tbl.currency
                    }
                    ).ToList();

                using (var _ctx = new RPT_IN_PAYMENT_INTERFACE())
                {

                    try
                    {
                        if (await _ctx.BulkingInsert(list))
                        {
                            Log.Write(String.Format($"{DateTime.Now} : \n LINHAS INSERIDAS COM SUCESSO \n "));
                        }
                        else
                        {
                            Log.Write(String.Format($"{DateTime.Now} : \n ERRO AO INSERIR LINHAS\n "));
                        }

                        File.Move(fName.FullName, backupPath + fName.Name.Replace(".csv","") + string.Format("{0:_yyyyMMdd}", DateTime.Now) + ".csv" );

                    }
                    catch (Exception ex)
                    {
                        Log.Write(String.Format($"{DateTime.Now} : \n ERRRO GERAL AO INSERIR LINHAS \n "));
                    }

                }

            }
        }
        public static async Task ImportCustomersFromCSV(string sourcePath, string backupPath, string fileName = "ExportOrder_Customers.csv")
        {
            FileInfo fName;
            string[] fileList = Directory.GetFiles(sourcePath, fileName);
            Log.Write(String.Format($"{DateTime.Now} : \n INÍCIO Importação Orders OCMS \n "));

            if (fileList.Any())
            {
                fName = new FileInfo(sourcePath + fileName);

                List<ExportOrderCustomer> ListCustomers = new List<ExportOrderCustomer>();

                foreach (var file in fileList)
                {

                    string[] contentFile = File.ReadLines(file, Encoding.GetEncoding("iso-8859-1")).Skip(1).ToArray();
                    foreach (var line in contentFile)
                    {
                        try
                        {
                            ExportOrderCustomer _exportOrderCustomer = new ExportOrderCustomer
                            {
                                type = line.Split(";")[0],
                                r_Tiers = line.Split(";")[1],
                                t_Etabcreation = line.Split(";")[2],
                                t_Juridique = line.Split(";")[3],
                                t_Libelle = line.Split(";")[4],
                                t_Prenom = line.Split(";")[5],
                                t_Adresse1 = line.Split(";")[6],
                                t_Adresse2 = line.Split(";")[7],
                                t_Adresse3 = line.Split(";")[8],
                                t_Ville = line.Split(";")[9],
                                t_Region = line.Split(";")[10],
                                t_CodePostal = line.Split(";")[11],
                                t_Pays = line.Split(";")[12],
                                t_Telephone = line.Split(";")[13],
                                t_Telex = line.Split(";")[14],
                                t_Telephone2 = line.Split(";")[15],
                                t_Email = line.Split(";")[16],
                                t_Journaissance = line.Split(";")[17],
                                t_MoisNaissance = line.Split(";")[18],
                                t_AnneeNaissance = line.Split(";")[19],
                                t_Sexe = line.Split(";")[20],
                                t_Etablissement = line.Split(";")[21],
                                t_Langue = line.Split(";")[22],
                                t_Auxiliaire = line.Split(";")[23],
                                t_NatureAuxi = line.Split(";")[24],
                                t_Devise = line.Split(";")[25],
                                t_Emailing = line.Split(";")[26],
                                ytc_CodeCpfCnpj = line.Split(";")[27],
                            };

                            ListCustomers.Add(_exportOrderCustomer);
                        }
                        catch (Exception ex)
                        {
                            Log.Write(String.Format($"{DateTime.Now} : \n Erro ao processar linha : {line.Split(';')[0]} \n Erro: {ex.InnerException}"));
                            continue;
                        }
                    }
                }

                var list = (

                    from tbl in ListCustomers
                    select new IN_CUSTOMER_INTERFACE
                    {
                        TYPE = tbl.type,
                        VIP_CODE = tbl.r_Tiers,
                        STORECODE = tbl.t_Etabcreation,
                        TITLE= tbl.t_Juridique,
                        LAST_NAME = tbl.t_Libelle,
                        FIRST_NAME = tbl.t_Prenom,
                        ADDRESS1 = tbl.t_Adresse1,
                        ADDRESS2 = tbl.t_Adresse2,
                        ADDRESS3 = tbl.t_Adresse3,
                        CITY = tbl.t_Ville,
                        STATE = tbl.t_Region,
                        POST_CODE = tbl.t_CodePostal,
                        COUNTRY = tbl.t_Pays,
                        PHONE = tbl.t_Telephone,
                        MOBILE = tbl.t_Telex,
                        PHONE3 = tbl.t_Telephone2,
                        EMAIL = tbl.t_Email,
                        DAY_BIRTH = tbl.t_Journaissance,
                        MONTH_BIRTH = tbl.t_MoisNaissance,
                        YEAR_BIRTH = tbl.t_AnneeNaissance,
                        SEX = tbl.t_Sexe,
                        T_ETABLISSEMENT = tbl.t_Etablissement,
                        T_LANGUE = tbl.t_Langue,
                        T_AUXILIAIRE = tbl.t_Auxiliaire,
                        T_NATUREAUXI = tbl.t_NatureAuxi,
                        T_DEVISE = tbl.t_Devise,
                        T_EMAILING = tbl.t_Emailing,
                        YTC_CODECPFCNPJ = tbl.ytc_CodeCpfCnpj
                    }
                    ).ToList();

                using (var _ctx = new RPT_IN_CUSTOMER_INTERFACE())
                {

                    try
                    {
                        if (await _ctx.BulkingInsert(list))
                        {
                            Log.Write(String.Format($"{DateTime.Now} : \n LINHAS INSERIDAS COM SUCESSO \n "));
                        }
                        else
                        {
                            Log.Write(String.Format($"{DateTime.Now} : \n ERRO AO INSERIR LINHAS\n "));
                        }

                        File.Move(fName.FullName, backupPath + fName.Name.Replace(".csv", "") + string.Format("{0:_yyyyMMdd}", DateTime.Now) + ".csv");

                    }
                    catch (Exception ex)
                    {
                        Log.Write(String.Format($"{DateTime.Now} : \n ERRRO GERAL AO INSERIR LINHAS \n "));
                    }

                }

            }
        }

    }
}
