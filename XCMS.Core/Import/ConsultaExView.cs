﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using XCMS.Data.Model.Integration;
using XCMS.Data.Model.Json.Get;
using XCMS.Data.Repository.Integration;

namespace XCMS.Core.Import
{
    public class ConsultaExView
    {
        
        public static async Task StatusPedidoAsync(Uri baseAddress, string Auth)
        {

            int contPedidosEncontrados = 0;
            int contPedidosInvalidos = 0;

            Log.Write(String.Format($"{DateTime.Now} : \n - Buscando lista de consulta \n "));
            var listaConsulta = new RPT_EXV_FILA_CONSULTA().Get().Where(p => p.CONSULTADO == false).Take(25).ToList();

            contPedidosEncontrados = listaConsulta.Count;

            Log.Write(String.Format($"{DateTime.Now} : \n - Encontrado(s) " + contPedidosEncontrados + " Pedido(s) para consultar\n"));

            var listaConsultaFinal = new List<EXV_FILA_CONSULTA>();

            List<EXV_STATUS_PEDIDO> listaValidos = new List<EXV_STATUS_PEDIDO>();

            foreach (var item in listaConsulta)
            {
                bool temBR; // PARA TRATAMENTO DOS CÓDIGOS QUE TEM BR0001
                temBR = item.CODIGO.Contains("BR0001-");

                var ret = await GetStatusPedido(baseAddress, Auth, codProduto: item.CODIGO.Replace("BR0001-", ""));

                if (ret.PEDIDO_EXTERNO != null)
                {
                    if (temBR)
                        ret.PEDIDO_EXTERNO = "BR0001-" + ret.PEDIDO_EXTERNO;

                    listaValidos.Add(ret);
                }

                item.CONSULTADO = true;
                item.DESC_ERRO = ret.PEDIDO_EXTERNO == null ? "Não Encontrado" : string.Empty;
                
                if(ret.PEDIDO_EXTERNO == null)
                {
                    contPedidosInvalidos++;
                }

                item.DATA_ALTERACAO = DateTime.Now;
                item.CADASTRADO_POR = "INTEGRACAO";

                listaConsultaFinal.Add(item);
            }

            using (var _ctx = new RPT_EXV_STATUS_PEDIDO())
            {
                var tblEXVSTATUSPEDIDO = _ctx.Get().ToList();
                foreach (var item in listaValidos)
                {
                    if (tblEXVSTATUSPEDIDO.Where(p => p.PEDIDO_EXTERNO == item.PEDIDO_EXTERNO).FirstOrDefault() != null)
                    {
                        var _detalhes = new RPT_EXV_DETALHE_PEDIDO_STATUS().Get().Where(p => p.PEDIDO_EXTERNO == item.PEDIDO_EXTERNO).ToList();
                        foreach (var det in _detalhes)
                        {
                            using (var del = new RPT_EXV_DETALHE_PEDIDO_STATUS())
                            {
                                del.Delete(det);
                            }
                        }
                        _ctx.Delete(item);
                    }
                    _ctx.Add(item);
                }
            }

            using (var _ctx = new RPT_EXV_FILA_CONSULTA())
            {
                foreach(var fila in listaConsultaFinal)
                {
                    _ctx.Edit(fila);
                }
            }

            if(contPedidosEncontrados > 0)
            {
                Log.Write(String.Format($"{DateTime.Now} : \n - Pedidos Encontrados: {contPedidosEncontrados - contPedidosInvalidos}\n"));
                Log.Write(String.Format($"{DateTime.Now} : \n - Pedidos NÃO encontrados: {contPedidosInvalidos}\n"));
            }
        }

        public static async Task<EXV_STATUS_PEDIDO> GetStatusPedido(Uri uri, string auth, string codProduto = "", string status = "")
        {
            CultureInfo culture = new CultureInfo("pt-BR", false);

            List<StatusPedido> statusPedidos;
            EXV_STATUS_PEDIDO resultadoExViews = new EXV_STATUS_PEDIDO();

            using (var httpClient = new HttpClient { BaseAddress = uri, Timeout = TimeSpan.FromMinutes(10) })
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", auth);
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                using (var response = await httpClient.GetAsync(string.Format("StatusPedido?Codigo={0}&Status={1}", codProduto, status)))
                {
                    var responseData = await response.Content.ReadAsStringAsync();
                    statusPedidos = JsonConvert.DeserializeObject<List<StatusPedido>>(responseData);

                    foreach (var item in statusPedidos)
                    {
                        foreach (var it in item.Header)
                        {

                            resultadoExViews.PEDIDO_EXTERNO = it.DetalhePedido.PedidoExterno.ToString();
                            resultadoExViews.TIPO_PEDIDO = it.TipoPedido == null ? "" : it.TipoPedido.ToString();
                            resultadoExViews.TIPO_ENTREGA = it.TipoEntrega == null ? "" : it.TipoEntrega.ToString();
                            resultadoExViews.CLIENTE = it.Cliente == null ? "" : it.Cliente.ToString();
                            resultadoExViews.ARMAZEM = it.Armazem == null ? "" : it.Armazem.ToString();
                            resultadoExViews.EXV_DETALHE_PEDIDO_STATUS = new List<EXV_DETALHE_PEDIDO_STATUS>();

                            foreach(var detalhe in it.DetalhePedido.DetalheStatus)
                            {
                                resultadoExViews.EXV_DETALHE_PEDIDO_STATUS.Add(new EXV_DETALHE_PEDIDO_STATUS
                                {
                                    PEDIDO_EXTERNO = it.DetalhePedido.PedidoExterno.ToString(),
                                    STATUS = detalhe.Status.ToString(),
                                    DATA = DateTime.Parse(detalhe.Data,culture),
                                    PEDIDO_WMS = it.DetalhePedido.PedidoWms.ToString(),
                                    STATUS_DESCRICAO = detalhe.StatusDescricao.ToString()
                                });
                            }
                        }
                    }
                }
            }

            return resultadoExViews;

        }

    }
}
