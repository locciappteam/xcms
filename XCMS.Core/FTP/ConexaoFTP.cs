﻿using FluentFTP;
using System;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;

namespace XCMS.Core.FTP
{
    public class ConexaoFTP : IDisposable
    {

        private readonly FtpClient FtpConn;
        public string ftpHost { get; set; }
        public int ftpPort { get; set; }
        public string ftpUser { get; set; }
        public string ftpPassword { get; set; }
        public string ftpPath { get; set; }
        public string localPath { get; set; }
        public string file { get; set; }

        public bool isConnected { get; set; }

        public ConexaoFTP(string ftpHost, int ftpPort, string ftpUser, string ftpPassword, string ftpPath, string localPath)
        {
            this.ftpHost = ftpHost;
            this.ftpPort = ftpPort;
            this.ftpUser = ftpUser;
            this.ftpPassword = ftpPassword;
            this.ftpPath = ftpPath;
            this.localPath = localPath;
            this.isConnected = false;

            NetworkCredential credential = new NetworkCredential(ftpUser, ftpPassword);
            FtpConn = new FtpClient(ftpHost, credential);
            FtpConn.Port = this.ftpPort;


            try
            {
                FtpConn.Connect();

                FtpConn.SetWorkingDirectory(ftpPath);

                if (FtpConn.IsConnected)
                {
                    this.isConnected = true;
                }
            }
            catch(Exception ex)
            {
                this.isConnected = false;
            }
            
        }

        public void Connect()
        {
            if (isConnected!)
            {
               
                try
                {
                    FtpConn.Connect();

                    if (FtpConn.IsConnected)
                    {
                        this.isConnected = true;
                    }
                }
                catch
                {
                    this.isConnected = false;
                }
            }
        }

        public void Disconnect()
        {

            try
            {
                if (isConnected)
                {
                    FtpConn.Disconnect();
                    isConnected = false;
                }
            }
            catch
            {

            }
        }

        public void DownloadFiles(string localPath, string fileType = "*.csv")
        {
            this.localPath = localPath;
            if (isConnected)
            {
                var fileList = FtpConn.GetListing().Where(p=> p.Name.Contains(fileType)).Select(p=> p.Name).ToList<String>();
                FtpConn.DownloadFiles(localPath, fileList);                
            }

        }

        public void Dispose()
        {
            FtpConn.Dispose();
        }
    }
}
